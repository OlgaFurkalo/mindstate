import re
import os
from pathlib import Path
from urllib.parse import urlparse
import shutil

import bibtexparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


class Url:
    def __init__(self, url):
        self.raw = url
        url_parts = urlparse(url)
        self.domain = url_parts.netloc
        self.path = url_parts.path


base_url = 'http://mindstate.info/links/'
wget_cmd = 'cd ../ext;wget -U mozilla --random-wait -q --domains=%s -p -k -e robots=off "%s"'
link_html = '''<!doctype html><html lang=ru><head><meta charset="utf-8"></head>
 <body>
  <p>Нажмите для перехода по <a href="%s">ссылке</a></p>
  <p>Если ссылка не открывается, то можете использовать <a href="%s">архивную копию</a></p>
 </body>
</html>
'''
links_dir = Path('site/links/')
if links_dir.exists():
    shutil.rmtree(links_dir)
os.makedirs(links_dir, exist_ok=True)

cache_dir = Path('../ext')
if not cache_dir.exists():
    cache_dir.mkdir()

def is_loaded(browser):
    status = browser.execute_script("return document.readyState")
    return True if status == "complete" else False

def get_browser():
    return webdriver.Firefox()

browser = get_browser()

with open('bibliography.bib') as bibtex_file:
    bibtex_database = bibtexparser.load(bibtex_file)

for bib in bibtex_database.entries:
    if 'url' not in bib:
        continue
    if re.search(r'mindstate.info', bib['url']):
        continue

    url = Url(bib['url'])
    print(f"Downloading: {url.raw} to cache")
    path = cache_dir / Path(url.domain + url.path)
    if not path.exists():
        if re.match(r'.+\.pdf$', url.path):
            command = wget_cmd % (url.domain, url.raw)
            print(command)
            os.system(command)
        else:
            browser.get(url.raw)
            WebDriverWait(browser, 10).until(lambda _: is_loaded(browser))
            html = browser.page_source
            html = html.replace('http_equiv', 'http-equiv')  # fix for expertcorps
            path.mkdir(parents=True, exist_ok=True)
            path = path / 'index.html'
            path.write_text(html)
    else:
        print('Reuse already downloaded')

    print('Creating link file')
    lnk_name = bib['ID'].lower() + '.html'
    link_file = links_dir / lnk_name
    cache_url = '/ext/' + url.domain + url.path
    link_file.write_text(link_html % (url.raw, cache_url))

    # replace real link to own file link
    bib['url'] = base_url + lnk_name

with open('auto.bib', 'w') as bibtex_file:
    bibtexparser.dump(bibtex_database, bibtex_file)

browser.quit()
