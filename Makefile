NAME = mindstate
HTML_DIR = site/b
FILE_DIR = site/download/
XSL_DIR = additions/xsl/
LOG_FILE = make.log

all: test clean prepare cache pdfs html htmlonepage odt plaintext epub3 mobi fb2 text4spell_check validate info
book: test clean prepare pdfa5 odt text4spell_check validate

clean:
	echo 'MAKE clean' | tee -a $(LOG_FILE)
	git clean -d -f -X >> $(LOG_FILE) 2>&1

cache:
	echo 'Download external links' | tee -a $(LOG_FILE)
	python3 additions/scripts/save_ext_links.py

test:
	py.test -v additions/scripts/test_book.py

prepare:
	echo 'MAKE dirs' | tee -a $(LOG_FILE)
	mkdir -p $(FILE_DIR)
	mkdir -p $(FILE_DIR)/html
	mkdir -p $(FILE_DIR)/pdf
	mkdir -p $(HTML_DIR)

prepdf: clean prepare cache

glossa4: prepdf
	echo 'MAKE glossary a4' | tee -a $(LOG_FILE)
	pdflatex $(NAME) >> $(LOG_FILE) 2>&1
	makeglossaries $(NAME) >> $(LOG_FILE) 2>&1

glossa5: prepdf
	echo 'MAKE glossary a5' | tee -a $(LOG_FILE)
	pdflatex $(NAME)-a5 >> $(LOG_FILE) 2>&1
	makeglossaries $(NAME)-a5 >> $(LOG_FILE) 2>&1

glossa6: prepdf
	echo 'MAKE glossary a6' | tee -a $(LOG_FILE)
	pdflatex $(NAME)-a6 >> $(LOG_FILE) 2>&1
	makeglossaries $(NAME)-a6 >> $(LOG_FILE) 2>&1

pdfs: pdfa4 pdfa5 pdfa6 pdfbooklet

pdfa4: glossa4
	echo 'CREATE pdf' | tee -a $(LOG_FILE)
	rubber --pdf $(NAME) >> $(LOG_FILE) 2>&1
	mv -f $(NAME).pdf $(FILE_DIR)

pdfa5: glossa5
	echo 'CREATE pdf A5' | tee -a $(LOG_FILE)
	rubber --pdf $(NAME)-a5 >> $(LOG_FILE) 2>&1
	cp -f $(NAME)-a5.pdf $(FILE_DIR)

pdfa6: glossa6
	echo 'CREATE pdf A6' | tee -a $(LOG_FILE)
	rubber --pdf $(NAME)-a6 >> $(LOG_FILE) 2>&1
	mv -f $(NAME)-a6.pdf $(FILE_DIR)

pdfbooklet:
	echo 'CREATE pdf booklet' | tee -a $(LOG_FILE)
	rubber --pdf $(NAME)-booklet >> $(LOG_FILE) 2>&1
	mv -f $(NAME)-booklet.pdf $(FILE_DIR)

docbook: cache
	echo 'CREATE docbook' | tee -a $(LOG_FILE)
	python3 additions/scripts/latex2docbook.py > $(NAME).xml
	xmllint --format --noblanks $(NAME).xml > $(NAME).docbook

validate: validate-docbook

validate-docbook: docbook
	xmlstarlet val --err --xsd /usr/share/xml/docbook/schema/xsd/5.0/docbook.xsd $(NAME).docbook

validate-fb2: fb2
	xmlstarlet val --err --xsd ~/fb2/FictionBook.xsd $(FILE_DIR)/$(NAME).fb2

format-fb2:
	xmllint --format --noblanks $(FILE_DIR)/$(NAME).fb2 > $(NAME).fb2

validate-epub: epub3
	epubcheck $(FILE_DIR)/$(NAME).epub 

html: docbook
	echo 'CREATE chunked HTML' | tee -a $(LOG_FILE)
	xsltproc --xinclude $(XSL_DIR)/html-params.xsl $(NAME).docbook >> $(LOG_FILE) 2>&1

htmlonepage: docbook
	echo 'CREATE one page HTML' | tee -a $(LOG_FILE)
	xsltproc --xinclude $(XSL_DIR)/html-onepage-params.xsl $(NAME).docbook > $(FILE_DIR)/html/$(NAME).html

plaintext: htmlonepage
	echo 'CREATE plain text' | tee -a $(LOG_FILE)
	elinks -dump -no-references $(FILE_DIR)/html/$(NAME).html > $(FILE_DIR)/$(NAME).txt

epub3:
	echo 'CREATE epub3' | tee -a $(LOG_FILE)
	mkdir -p epub
	xsltproc --xinclude $(XSL_DIR)/epub3-params.xsl $(NAME).docbook >> $(LOG_FILE) 2>&1
	cd epub; zip -0Xq ../$(FILE_DIR)/$(NAME).epub mimetype >> $(LOG_FILE) 2>&1
	cd epub; zip -Xr9D ../$(FILE_DIR)/$(NAME).epub META-INF OEBPS >> $(LOG_FILE) 2>&1

mobi:
	echo 'CREATE mobi' | tee -a $(LOG_FILE)
	mkdir -p epub
	xsltproc --xinclude $(XSL_DIR)/epub3-4-mobi-params.xsl $(NAME).docbook >> $(LOG_FILE) 2>&1
	cd epub; zip -0Xq $(NAME).epub mimetype >> $(LOG_FILE) 2>&1
	cd epub; zip -Xr9D $(NAME).epub META-INF OEBPS >> $(LOG_FILE) 2>&1
	mv epub/$(NAME).epub .
	~/bin/kindlegen $(NAME).epub && ([ $$? -eq 0 ] && echo "success!") || echo "failure!" >> $(LOG_FILE) 2>&1
	mv $(NAME).mobi $(FILE_DIR)/$(NAME).mobi

odt: htmlonepage
	echo 'CREATE odt' | tee -a $(LOG_FILE)
	libreoffice --headless --convert-to odt $(FILE_DIR)/html/$(NAME).html >> $(LOG_FILE) 2>&1
	mv $(NAME).odt $(FILE_DIR)

fb2: docbook
	echo 'CREATE fb2' | tee -a $(LOG_FILE)
	java -jar ~/.saxon/saxon9he.jar -o:$(FILE_DIR)/$(NAME).fb2 $(NAME).docbook $(XSL_DIR)/docbook2fb2.xsl

text4spell_check: plaintext
	cat $(FILE_DIR)/$(NAME).txt > text4spell_check.txt
	find site/ -maxdepth 1 -name '*.html' -exec elinks -dump -no-references {} >> text4spell_check.txt \;

links_check:
	linkchecker --ignore-url=^mailto: --ignore-url=http://mindstate.info/ext http://mindstate.info

links_check_bib:
	linkchecker --ignore-url=^mailto: -r2 http://mindstate.info/b/bi01.html

info:
	python3 -c "import subprocess;import re;result = subprocess.run(['detex', 'main.tex'], stdout=subprocess.PIPE);print('Symbols: ', len(result.stdout.decode('utf8')))"
	python3 -c "import subprocess;import re;result = subprocess.run(['detex', 'main.tex'], stdout=subprocess.PIPE);words = result.stdout.decode('utf8').split();print('Reading time: ', (len(words)/180)/60)"
	python3 -c "import subprocess; result = subprocess.run(['detex', 'main.tex'], stdout=subprocess.PIPE); print('Author lists: ', len(result.stdout.decode('utf8'))/40000)"

deps:
	wajig install texlive-full calibre elinks rubber linkchecker lftp docbook5-xml docbook-xsl-ns xsltproc libxml2-utils xmlstarlet epubcheck
	pip3 install --user bibtexparser in_place selenium

remark_terms:
	python3 additions/scripts/remove_terms.py
	python3 additions/scripts/mark_terms.py

.SILENT:
